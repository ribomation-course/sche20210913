#!/usr/bin/env bash
set -eux

wget -O mysql_all.deb https://dev.mysql.com/get/mysql-apt-config_0.8.19-1_all.deb
sudo DEBIAN_FRONTEND=noninteractive dpkg -i mysql_all.deb
rm -rf mysql_all.deb
sudo apt update

sudo DEBIAN_FRONTEND=noninteractive apt install --yes mysql-server

sudo mysql --user=root <<"EOT"
ALTER USER root@localhost IDENTIFIED WITH caching_sha2_password BY 'tjollahopp';
UPDATE mysql.user SET host = '%' WHERE user = 'root';
FLUSH PRIVILEGES;
CREATE DATABASE test;
EOT

mysql --user=root --password=tjollahopp test<<"EOT"
create table persons (
    id      int primary key auto_increment,
    name    varchar(32) not null,
    age     int not null default 18
);

insert into persons (name,age) values 
    ('Anna Conda', 42), 
    ('Per Silja', 33), 
    ('Inge Vidare', 52);

select * from persons;
EOT
