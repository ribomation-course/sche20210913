$(() => {
    'use strict';
    const baseUrl = 'https://randomuser.me/api/';

    function mkRow(seed, row, size, person) {
        const firstName  = person.name.first;
        const lastName   = person.name.last;
        const salutation = person.name.title;
        const email      = person.email;
        const city       = person.location.city;
        const country    = person.location.country;
        const image      = person.picture.thumbnail;

        return `
            <tr>
                <td>
                    <img src="${image}" class="img-thumbnail" alt="${firstName} ${lastName}" />
                </td>
                <td>
                    <em>${salutation}</em> ${firstName} ${lastName}
                </td>
                <td>
                    <strong>${city}</strong>, ${country}
                </td>
                <td>
                    <code>${email}</code>
                </td>
                <td>
                    <a class="btn btn-primary btn-sm" href="./detail.html?seed=${seed}&row=${row}&size=${size}" >SHOW</a>
                </td>
            </tr>
        `;
    }

    function genSeed() {
        const ts = Date.now().toString(16);
        return `app-${ts}`;
    }

    function load(numberOfPersons) {
        const seed = genSeed();
        $.ajax({
            url: `${baseUrl}?seed=${seed}&results=${numberOfPersons}&nat=us,gb,no,dk`,
            dataType: 'json',
            success: (payload) => {
                const seed = payload.info.seed;
                $('#list').empty();
                payload.results.forEach((person, idx) => {
                    const html = mkRow(seed, idx, numberOfPersons, person);
                    $('#list').append(html);
                });
            }
        });
    }

    $('#load').click((ev) => {
        ev.preventDefault();
        const count = +$('#count').val() || 5;
        load(count);
    });

    $('#count').change((ev) => {
        ev.preventDefault();
        const count = +$('#count').val() || 5;
        load(count);
    });


    const N = 5;
    $('#count').val(N);
    load(N);
});
