const baseUrl = 'http://16.170.159.227:8080/api/person'

const app = {
    message: '',
    items: [],
    editId: undefined,
}

function populateAll() {
    const tbody = document.querySelector('#items')
    tbody.innerHTML = ''
    app.items.forEach(item => {
        const tr = createTR(item)
        tbody.appendChild(tr)
        tr.querySelector('.remove')
            .addEventListener('click', async (ev) => {
                ev.preventDefault()
                remove(item)
            })
        tr.querySelector('.edit')
            .addEventListener('click', (ev) => {
                ev.preventDefault()
                edit(item)
            })
    })
}

function createTR(item) {
    const tr = document.createElement('tr')
    tr.setAttribute('data-id', item.id)
    tr.innerHTML = `
        <td> ${item.id} </td>
        <td> ${item.first_name} </td>
        <td> ${item.last_name} </td>
        <td> ${item.email} </td>
        <td> ${item.gender} </td>
        <td>  
            <button class="edit"> i </button>
            <button class="remove"> X </button>
        </td>
    `
    return tr
}

function clear() {
    document.querySelector('#first_name').value = ''
    document.querySelector('#last_name').value = ''
    document.querySelector('#email').value = ''
    document.querySelector('#female').checked = false
    app.editId = undefined
}

function edit(item) {
    document.querySelector('#first_name').value = item.first_name
    document.querySelector('#last_name').value = item.last_name
    document.querySelector('#email').value = item.email
    document.querySelector('#female').checked = (item.gender === 'Female')
    app.editId = item.id
}

async function loadAll() {
    const res = await fetch(baseUrl)
    if (res.ok) {
        app.items = await res.json()
        populateAll()
    } else {
        app.message = 'LoadAll failed'
    }
}

async function remove(item) {
    const ix = app.items.findIndex(x => x.id === item.id)
    if (ix > -1) {
        const res = await fetch(`${baseUrl}/${item.id}`, {
            method: 'DELETE'
        })
        if (res.ok) {
            app.items.splice(ix, 1)
            const tbody = document.querySelector('#items')
            const tr = tbody.querySelector(`tr[data-id='${item.id}']`)
            tbody.removeChild(tr)
        } else {
            app.message = 'remove failed'
        }
    }
}

async function save() {
    const payload = {
        first_name: document.querySelector('#first_name').value,
        last_name: document.querySelector('#last_name').value,
        email: document.querySelector('#email').value,
        gender: document.querySelector('#female').checked ? 'Female' : 'Male',
    }
    const body = JSON.stringify(payload)
    const headers = { 'Content-Type': 'application/json' }

    let url, method
    if (app.editId) {
        url = `${baseUrl}/${app.editId}`
        method = 'PUT'
    } else {
        url = baseUrl
        method = 'POST'
    }

    const res = await fetch(url, { method, headers, body })
    if (res.ok) {
        clear()
        await loadAll()
    } else {
        app.message = 'save failed'
    }
}

document.querySelector('.clear')
    .addEventListener('click', (ev) => {
        ev.preventDefault()
        clear()
    })

document.querySelector('.save')
    .addEventListener('click', async (ev) => {
        ev.preventDefault()
        save()
    })

loadAll().then(ok => true)
