# Instructions

## Upload files
Upload the asset files to the api server, then login

    scp -i aws.pem app.js index.html people.jpg style.css ubuntu@IP:.
    ssh -i aws.pem ubuntu@IP

## Update the API URL
Open the app.js file and set the correct value for `baseUrl`.

## Install software
Install nginx and copy the assets to the www directory

    sudo apt update
    sudo apt install --yes nginx
    sudo cp app.js index.html people.jpg style.css /var/www/html
