# Instructions

## Upload files
Upload the JSON files to each asset server, then login

    scp -i aws.pem *.json ubuntu@IP:.
    ssh -i aws.pem ubuntu@IP

## Install software
Install Node.js and NPM

    sudo apt update
    sudo apt install --yes nodejs npm

## Install dependencies
Install json-server

    npm install

## Launch API-Server
Launch the API server and keep it running in a terminal window

    npm run start
