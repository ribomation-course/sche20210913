# Installation Instructions

In order to participate and perform the programming exercises of the course, 
you need to have the following installed/configured.

**Ensure you have permissions to install software on your laptop.**

# CLI Tools
It's strongly recommended that you do the exercises in a BASH 
like environment, such as `WSL @ Windows 10`, `BASH for Windows GIT`,
Linux running in a _Virtual Machine_ or _Docker_ or similar.

The primary tools you need for the exercises of the course:
* SSH Client (_to login to an EC2 server_)
* SCP Client (_to copy files to an EC2 server_)
* AWS CLI (_interacting with AWS from the command-line_)
* A text editor
* A modern browser, such as Chrome, Edge or FireFox

For some exercises it is advisable to have the following installed
* Node.js version 10.x, with NPM
* Database client


If you have a *NIX like environment you probably already have
access to `ssh`, `scp` and `zip`, else you need to find another solution,
such as PuTTY or similar.

We will install the AWS CLI during the course. It's an Windows installer,
that unpacks a bundled Python environment together with the
CLI tool. For *NIX environment, install first Python and PIP, then use
PIP to install the CLI. Anyway, we do it during the course.

Install  [Microsoft Visual Code](https://code.visualstudio.com/) 
or another IDE or text editor, before the course starts.

Also install [Node.js, version 10.x](https://nodejs.org/en/download/),
before the course starts.

Finally, install a DB client such as
[HeidiSQL DB Client](https://www.heidisql.com/) or [JetBrains DataGrip (30 days trial) ](https://www.jetbrains.com/datagrip/) or use one you are more familiar with. 
Anyway, it must be able to connect to a remote MySQL database.


# Proxy and/or VPN
If your laptop is using any corporate network software, that limits
your connectivity. You need to ensure you can perform SSH access (port 22)
to AWS and connect to a MySQL DB (port 3306), plus port 80 and 8080.


# AWS Account
In order to interact with AWS and perform the exercises, you need
to sign-up for an AWS account. Please, do this before the course
starts. You probably need to provide a credit/debit card to cover
any excess spending. 
* [Sign-Up for AWS](https://portal.aws.amazon.com/billing/signup#/start)

The exercises in the course are not intended to cost anything. In
most cases, you will be using the AWS Free Tier.

Just remember to shut down any running services, that are charged
per hour, such as EC2 servers and RDS databases.

